/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tryonics.android.taggedlk.activity;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tryonics.android.taggedlk.R;
import com.tryonics.android.taggedlk.fragment.EventFragment;
import com.tryonics.android.taggedlk.fragment.PromotionFragment;
import com.tryonics.android.taggedlk.locationservices.LocationService;
import com.tryonics.android.taggedlk.model.Event.Event;
import com.tryonics.android.taggedlk.model.Promotion.Promotion;
import com.tryonics.android.taggedlk.util.Constant;

public class MainActivity extends FragmentActivity implements ActionBar.TabListener, PromotionFragment.PromotionFragmentOnClick, EventFragment.EventFragmentOnClick {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the
     * three primary sections of the app. We use a {@link android.support.v4.app.FragmentPagerAdapter}
     * derivative, which will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    AppSectionsPagerAdapter mAppSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will display the three primary sections of the app, one at a
     * time.
     */
    ViewPager mViewPager;
    int i = 1;
    boolean flag = false;
    private boolean currentPlace = true;
    private Bundle mSavedState;
    // /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // This is used to the location service of the Location Manager
    private boolean mIsBound;
    private LocationService mBoundService;


    ///// location
    private Intent intentNearByLocations;
    private LocationReceiver locationReceiver = null;
    private Location location;


    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service. Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.

            mBoundService = ((LocationService.LocalBinder) service).getService(
                    MainActivity.this, LocationService.PLACES_ACTIVITY);

            // Tell the user about this for our demo.
            // Toast.makeText(PlacesActivity.this, "Service Conected Places",
            // Toast.LENGTH_SHORT).show();
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            mBoundService = null;
            // Toast.makeText(PlacesActivity.this, "Service Disconected Places",
            // Toast.LENGTH_SHORT).show();
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create the adapter that will return a fragment for each of the three primary sections
        // of the app.
        mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();

        // Specify that the Home/Up button should not be enabled, since there is no hierarchical
        // parent.
        actionBar.setHomeButtonEnabled(false);

        // Specify that we will be displaying tabs in the action bar.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Set up the ViewPager, attaching the adapter and setting up a listener for when the
        // user swipes between sections.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When swiping between different app sections, select the corresponding tab.
                // We can also use ActionBar.Tab#select() to do this if we have a reference to the
                // Tab.
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by the adapter.
            // Also specify this Activity object, which implements the TabListener interface, as the
            // listener for when this tab is selected.
            switch (i) {
                case 0:
                    actionBar.addTab(
                            actionBar.newTab()
                                    .setTag("PROMOTION")
                                    .setText("")
                                    .setIcon(R.drawable.tab_promotion_icon)
                                    .setTabListener(this));
                    break;
                case 1:
                    actionBar.addTab(
                            actionBar.newTab()
                                    .setText("")
                                    .setIcon(R.drawable.tab_event_icon)
                                    .setTabListener(this));
                    break;
                case 2:
                    /*actionBar.addTab(
                            actionBar.newTab()
                                    .setText("BECON")
                                    .setIcon(R.drawable.ic_launcher)
                                    .setTabListener(this));*/
                    actionBar.addTab(
                            actionBar.newTab().setCustomView(R.layout.tab_layout)
                                    .setTabListener(this));
                    break;
            }

            /*actionBar.addTab(
                    actionBar.newTab()
                            .setText(mAppSectionsPagerAdapter.getPageTitle(i))
                            .setIcon(R.drawable.ic_launcher)
                            .setTabListener(this));*/

        }

        TextView count = (TextView) findViewById(R.id.tab_count);
        count.setText("5");

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    void doBindService() {
        // Establish a connection with the service. We use an explicit
        // class name because we want a specific service implementation that
        // we know will be running in our own process (and thus won't be
        // supporting component replacement by other applications).

        try {

            intentNearByLocations = new Intent(MainActivity.this,
                    LocationService.class);
            getApplicationContext().bindService(intentNearByLocations,
                    mConnection, Context.BIND_AUTO_CREATE);

        } catch (Exception e) {
            e.printStackTrace();
        }
        mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            // Detach our existing connection.
            getApplicationContext().unbindService(mConnection);
            mIsBound = false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter movementFilter;
        movementFilter = new IntentFilter(Intent.ACTION_VIEW);
        locationReceiver = new LocationReceiver();
        registerReceiver(locationReceiver, movementFilter);

        doBindService();
    }

    protected void onPause() {
        super.onPause();
        // in this method we have to stop all the functinalities quickly.This is
        // called when the Activity is going to pause for another Activity to start
        doUnbindService();
        unregisterReceiver(locationReceiver);
    }

    @Override
    public void PromotionOnClick(Promotion promotion) {
        Intent intent = new Intent(getApplicationContext(), PromotionDetailActivity.class);
        intent.putExtra(Constant.PROMOTION, promotion);
        startActivity(intent);
    }

    @Override
    public void PromotionPullToDownRefresh() {
        PromotionFragment msfd = (PromotionFragment) getSupportFragmentManager().findFragmentByTag(mAppSectionsPagerAdapter.getFragmentTag(0));
        msfd.setLocation(location);
    }

    @Override
    public void EventOnClick(Event event) {
        Toast.makeText(getApplicationContext(), event.getTitle(), Toast.LENGTH_LONG).show();
    }


    /*Fragment Callback methods*/

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
     * sections of the app.
     */

    public static class AppSectionsPagerAdapter extends FragmentPagerAdapter {

        public AppSectionsPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    Fragment promotionFragment = new PromotionFragment();
                    return promotionFragment;
                case 1:
                    //return new EventFragment();
                    Fragment eventFragment = new EventFragment();
                   /* Bundle eventFragmentArgs = new Bundle();
                    eventFragmentArgs.putInt(DummySectionFragment.ARG_SECTION_NUMBER, i + 5);
                    eventFragment.setArguments(eventFragmentArgs);*/
                    return eventFragment;
                case 2:
                    // The other sections of the app are dummy placeholders.
                    Fragment fragment1 = new DummySectionFragment();
                    Bundle args1 = new Bundle();
                    args1.putInt(DummySectionFragment.ARG_SECTION_NUMBER, i + 5);
                    fragment1.setArguments(args1);
                    return fragment1;

            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Section " + (position + 1);
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        private String getFragmentTag(int pos) {
            return "android:switcher:" + R.id.pager + ":" + pos;
        }

    }

    /**
     * A dummy fragment representing a section of the app, but that simply displays dummy text.
     */
    public static class DummySectionFragment extends Fragment {

        public static final String ARG_SECTION_NUMBER = "section_number";

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_collection_object, container, false);
            Bundle args = getArguments();
            ((TextView) rootView.findViewById(android.R.id.text1)).setText(
                    getString(R.string.dummy_section_text, args.getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    class LocationReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle incoming = intent.getExtras();

            location = (Location) incoming.get("location");

            Toast.makeText(getApplicationContext(), "location update " + i++ + " " + location.getLatitude() + " " + location.getLongitude(),
                    Toast.LENGTH_SHORT).show();

            if (location != null) {

                if (!flag) {
                    /*Toast.makeText(getApplicationContext(), "LLL " + location.getLatitude() + " " + location.getLongitude(),
                            Toast.LENGTH_SHORT).show();*/

                    PromotionFragment msfd = (PromotionFragment) getSupportFragmentManager().findFragmentByTag(mAppSectionsPagerAdapter.getFragmentTag(0));
                    msfd.setLocation(location);
                    flag = true;
                }


            } else {

                Toast.makeText(getApplicationContext(), "Location error",
                        Toast.LENGTH_SHORT).show();
            }
            // loadNearByPlaces();
            // activityCounter.instance.updateUI(counterValue);
        }
    }
}

package com.tryonics.android.taggedlk.model.Promotion;

import android.os.Parcel;
import android.os.Parcelable;

import com.tryonics.android.taggedlk.model.Error;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;

/**
 * Created by tryonics on 9/29/2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PromotionResponse implements Parcelable {

    @JsonProperty("error")
    private com.tryonics.android.taggedlk.model.Error error;
    @JsonProperty("prromotions")
    private ArrayList<Promotion> promotion;
    @JsonProperty("offset")
    private String offset;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public ArrayList<Promotion> getPromotionList() {
        return promotion;
    }

    public void setPromotionList(ArrayList<Promotion> promotion) {
        this.promotion = promotion;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.error, 0);
        dest.writeSerializable(this.promotion);
        dest.writeString(this.offset);
    }

    public PromotionResponse() {
    }

    private PromotionResponse(Parcel in) {
        this.error = in.readParcelable(Error.class.getClassLoader());
        this.promotion = (ArrayList<Promotion>) in.readSerializable();
        this.offset = in.readString();
    }

    public static final Parcelable.Creator<PromotionResponse> CREATOR = new Parcelable.Creator<PromotionResponse>() {
        public PromotionResponse createFromParcel(Parcel source) {
            return new PromotionResponse(source);
        }

        public PromotionResponse[] newArray(int size) {
            return new PromotionResponse[size];
        }
    };
}

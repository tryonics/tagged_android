package com.tryonics.android.taggedlk.model.Event;

import android.os.Parcel;
import android.os.Parcelable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by tryonics on 9/30/2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Event implements Parcelable {

    @JsonProperty("event_id")
    private String eventID;
    @JsonProperty("day")
    private String day;
    @JsonProperty("title")
    private String title;
    @JsonProperty("date")
    private String date;
    @JsonProperty("time")
    private String time;
    @JsonProperty("place")
    private String place;
    @JsonProperty("distance")
    private String distance;

    public String getEventID() {
        return eventID;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.eventID);
        dest.writeString(this.day);
        dest.writeString(this.title);
        dest.writeString(this.date);
        dest.writeString(this.time);
        dest.writeString(this.place);
        dest.writeString(this.distance);
    }

    public Event() {
    }

    private Event(Parcel in) {
        this.eventID = in.readString();
        this.day = in.readString();
        this.title = in.readString();
        this.date = in.readString();
        this.time = in.readString();
        this.place = in.readString();
        this.distance = in.readString();
    }

    public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {
        public Event createFromParcel(Parcel source) {
            return new Event(source);
        }

        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
}

package com.tryonics.android.taggedlk.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;


/**
 * Created by dimalperera on 8/7/14.
 */
public class EmptyView extends LinearLayout{

    private Context context;


    public EmptyView(Context context) {
        super(context);
        this.context = context;
        m();
    }

    public EmptyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        m();
    }

    public EmptyView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        m();
    }


    public void m() {
        //inflate(context, R.layout.layout_empty_place, this);
    }
}

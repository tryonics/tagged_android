package com.tryonics.android.taggedlk.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import com.tryonics.android.taggedlk.R;
import com.tryonics.android.taggedlk.util.NetworkConnection;

import java.util.Timer;

public class ActivitySplash extends Activity{


    private int REQUEST_CODE = 1;
    Button btnConInternet;
    Timer timer;
    private final int SPLASH_DISPLAY_LENGTH = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        btnConInternet = (Button) findViewById(R.id.btnConInternet);
        btnConInternet.setVisibility(View.INVISIBLE);
        btnConInternet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkNetworkAndGpsConnection();
            }
        });

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                checkNetworkAndGpsConnection();
            }
        }, SPLASH_DISPLAY_LENGTH);

    }

    private void checkNetworkAndGpsConnection(){
       NetworkConnection networkConnection = new NetworkConnection(getApplicationContext());
        networkConnection.setListener(new NetworkConnection.NetworkConnectionStatus() {
            @Override
            public void connectionSuccessful() {


                GpsEnable();
            }

            @Override
            public void connectionFail() {
                Toast.makeText(getApplicationContext(), "INTERNET ERROR", Toast.LENGTH_SHORT).show();
                btnConInternet.setVisibility(View.VISIBLE);
                btnConInternet.setText("Please retry. No internet connection");
            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            switch (requestCode) {
                case 1:
                    GpsEnable();
                    break;
            }
        }
    }


    private void  GpsEnable(){
        LocationManager service = (LocationManager) getSystemService(getApplicationContext().LOCATION_SERVICE);
        boolean enabled = service
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!enabled) {

            btnConInternet.setVisibility(View.VISIBLE);
            btnConInternet.setText("Please retry. No GPS connection");

            final AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySplash.this);

            builder.setMessage("Tagged needs access to your location. Please turn on location access.")
                    .setTitle("Location services disabled");


            builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUEST_CODE);

                }
            });

            builder.setNegativeButton("Ignore", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    btnConInternet.setVisibility(View.VISIBLE);
                }
            });

            AlertDialog dialog = builder.create();

            dialog.show();


        }else{

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
}

package com.tryonics.android.taggedlk.util;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;


/**
 * Created by Dimal Perera on 9/7/2014.
 */
public class ToastMessage {

    public static void make(Activity activity, String message){
        if(Constant.DEBUG)
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    public static void make(Context activity, String message){
        if(Constant.DEBUG)
            Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

}

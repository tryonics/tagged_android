package com.tryonics.android.taggedlk.model.promotiondetail;

import android.os.Parcel;
import android.os.Parcelable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;

/**
 * Created by tryonics on 10/7/2014.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PromotionDetail implements Parcelable {

    @JsonProperty("title")
    private String title;
    @JsonProperty("description")
    private String description;
    @JsonProperty("expiry_date")
    private String expiryDate;
    @JsonProperty("image")
    private String imageUrl;
    @JsonProperty("sub_content")
    private ArrayList<PromotionDetailSubContent> promotionDetailSubContent;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ArrayList<PromotionDetailSubContent> getPromotionDetailSubContent() {
        return promotionDetailSubContent;
    }

    public void setPromotionDetailSubContent(ArrayList<PromotionDetailSubContent> promotionDetailSubContent) {
        this.promotionDetailSubContent = promotionDetailSubContent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.description);
    }

    public PromotionDetail() {
    }

    private PromotionDetail(Parcel in) {
        this.title = in.readString();
        this.description = in.readString();
    }

    public static final Parcelable.Creator<PromotionDetail> CREATOR = new Parcelable.Creator<PromotionDetail>() {
        public PromotionDetail createFromParcel(Parcel source) {
            return new PromotionDetail(source);
        }

        public PromotionDetail[] newArray(int size) {
            return new PromotionDetail[size];
        }
    };
}

package com.tryonics.android.taggedlk.model.promotiondetail;

import android.content.Context;
import android.util.Log;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.tryonics.android.taggedlk.model.Promotion.Promotion;

import java.util.HashMap;


/**
 * Created by dimalperera on 7/30/14.
 */
public class PromotionDetailRequest extends
        SpringAndroidSpiceRequest<PromotionDetailResponse> {

    private Promotion promotion;
    private String placeUrlWithoutCategory = "http://192.168.1.203/projects/tag/public/v1/promotion/show/1";


    public PromotionDetailRequest(Context mContext, Promotion promotion) {
        super(PromotionDetailResponse.class);
        this.promotion = promotion;

        Log.e("promotion ID", promotion.getPromotionId());


    }

    @Override
    public PromotionDetailResponse loadDataFromNetwork() throws Exception {

        PromotionDetailResponse placeResponse;

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("promotionId", promotion.getPromotionId());

        placeResponse = getRestTemplate().getForObject(placeUrlWithoutCategory, PromotionDetailResponse.class);
        return placeResponse;
    }

    public String createCacheKey() {
        return "promotion ID/"+promotion.getPromotionId();
    }
}

package com.tryonics.android.taggedlk.fragment;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.tryonics.android.taggedlk.R;
import com.tryonics.android.taggedlk.adapter.PromotionAdapter;
import com.tryonics.android.taggedlk.model.Promotion.Promotion;
import com.tryonics.android.taggedlk.model.Promotion.PromotionResponse;
import com.tryonics.android.taggedlk.model.Promotion.PromotionRequest;
import com.tryonics.android.taggedlk.util.NetworkConnection;

/**
 * Created by tryonics on 9/29/2014.
 */
public class PromotionFragment extends Fragment {

    private PullToRefreshListView promotionPullToRefreshListView;
    private SpiceManager spiceManager = new SpiceManager(JacksonSpringAndroidSpiceService.class);
    private PromotionAdapter promotionAdapter;
    private PromotionRequest request;
    private String lastRequestCacheKey;
    private String offset = "0";
    private Location location;
    private PromotionFragmentOnClick promotionFragmentOnClick;

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_promotion, container, false);

        initUIComponents(rootView);

        this.rootView = rootView;



       /* // Demonstration of a collection-browsing activity.
        rootView.findViewById(R.id.demo_collection_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), PromotionDetailActivity.class);
                        startActivity(intent);
                    }
                });

        // Demonstration of navigating to external activities.
        rootView.findViewById(R.id.demo_external_activity)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Create an intent that asks the user to pick a photo, but using
                        // FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET, ensures that relaunching
                        // the application from the device home screen does not return
                        // to the external activity.
                        Intent externalActivityIntent = new Intent(Intent.ACTION_PICK);
                        externalActivityIntent.setType("image*//*");
                        externalActivityIntent.addFlags(
                                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        startActivity(externalActivityIntent);
                    }
                });*/

        return rootView;
    }

    private void initUIComponents(View rootView) {


        promotionPullToRefreshListView = (PullToRefreshListView)rootView.findViewById(R.id.promotionPullToRefreshListView);
        promotionPullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(
                    final PullToRefreshBase<ListView> refreshView) {

                offset = "0"; //Set off as zero to get init request
                promotionAdapter.clearPlaceList();
                promotionFragmentOnClick.PromotionPullToDownRefresh();

               /* NetworkConnection networkConnection = new NetworkConnection(getActivity());
                networkConnection.setListener(new NetworkConnection.NetworkConnectionStatus() {
                    @Override
                    public void connectionSuccessful() {
                        offset = "0"; //Set off as zero to get init request
                        promotionAdapter.clearPlaceList();
                        promotionFragmentOnClick.PullToDownRefresh();
                    }

                    @Override
                    public void connectionFail() {
                        promotionPullToRefreshListView.onRefreshComplete();
                       // Crouton.makeText(getActivity(), "No internet connection", Style.ALERT).show();
                    }
                });*/

            }

            @Override
            public void onPullUpToRefresh(
                    final PullToRefreshBase<ListView> refreshView) {

                /*NetworkConnection networkConnection = new NetworkConnection(getActivity());
                networkConnection.setListener(new NetworkConnection.NetworkConnectionStatus() {
                    @Override
                    public void connectionSuccessful() {
                       // performRequest(category, currentLocation, offset);
                    }

                    @Override
                    public void connectionFail() {
                        promotionPullToRefreshListView.onRefreshComplete();
                        //Crouton.makeText(getActivity(), "No internet connection", Style.ALERT).show();
                    }
                });*/

                performRequest(location, offset);

            }


        });

        promotionAdapter = new PromotionAdapter(getActivity());
        promotionPullToRefreshListView.setAdapter(promotionAdapter);

        promotionAdapter.setOnItemOnclick(new PromotionAdapter.PromotionItemOnClick() {
            @Override
            public void onClickItem(Promotion promotion) {
                promotionFragmentOnClick.PromotionOnClick(promotion);
            }
        });

       /* emptyView = new EmptyView(getActivity());
        pullToRefreshListView.setEmptyView(emptyView);*/

       /* if(category != null)
            placeAdapter.clearPlaceList();*/
        //Toast.makeText(getActivity(), "XXX", Toast.LENGTH_SHORT).show();

      //  performRequest(offset);

    }

    @Override
    public void onStart() {
        if (!spiceManager.isStarted()) {
            spiceManager.start(getActivity());
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (spiceManager.isStarted()) {
            spiceManager.shouldStop();
        }
        super.onStop();
    }

    private void performRequest(Location location, String offset) {
        request = new PromotionRequest(getActivity(),location, offset);
        lastRequestCacheKey = request.createCacheKey();
        spiceManager.execute(request, lastRequestCacheKey,
                DurationInMillis.ALWAYS_EXPIRED, new ListPlaceRequestListener());
    }

    private class ListPlaceRequestListener implements RequestListener<PromotionResponse> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {

        }

        @Override
        public void onRequestSuccess(PromotionResponse promotionResponse) {
            if (promotionResponse.getPromotionList() == null) {
                return;
            }

            //pullToRefreshListView.setScrollEmptyView(true);

            promotionAdapter.appendPlaceList(promotionResponse.getPromotionList());

            offset = promotionResponse.getOffset();

            if (offset.equals("0")) {
                promotionPullToRefreshListView.onRefreshComplete();
                promotionPullToRefreshListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
            } else {
                promotionPullToRefreshListView.onRefreshComplete();
                promotionPullToRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);
            }


        }


    }

    public interface PromotionFragmentOnClick {
        public void PromotionOnClick(Promotion promotion);
        public void PromotionPullToDownRefresh();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            promotionFragmentOnClick = (PromotionFragmentOnClick) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }


    public void setLocation(Location location){
        this.location = location;
        performRequest(location,offset);
    }

}
/*
 * Copyright 2011 Cyber LMJ
 *
 * @author Dulan Dissanayake
 * @version 1.0
 * 
 * 
 */

package com.tryonics.android.taggedlk.locationservices;



import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;



// Need the following import to get access to the app resources, since this
// class is in a sub-package.
//import com.example.android.apis.R;

/**
 * This is an example of implementing an application service that runs locally
 * in the same process as the application. The
 * { LocalServiceActivities.Controller} and
 * { LocalServiceActivities.Binding} classes show how to interact with the
 * service.
 * 
 * <p>
 * Notice the use of the {@link android.app.NotificationManager} when interesting things
 * happen in the service. This is generally how background services should
 * interact with the user, rather than doing something more disruptive such as
 * calling startActivity().
 */

public class LocationService extends Service {
	// private NotificationManager mNM;
	private LalinksGeoLocation lalinksGeoLocationObj = null;

	public static final String PLACES_ACTIVITY = "com.sitv.android.fourdirection.MainActivity";
	/*public static final String OFFERS_ACTIVITY = "com.lalinks.android.activities.AdsActivity";
	public static final String PEOPLE_ACTIVITY = "com.lalinks.android.activities.PeopleActivity";
	public static final String LIST_ACTIVITY = "com.lalinks.android.activities.ListActivity";
	public static final String MERCHANTPROFILE_ACTIVITY = "com.lalinks.android.activities.MerchantProfileActivity";*/

	public String sActivityGlobal = "";
	public Context contextGlobal = null;
	private Location previousLocation = null;

	/**
	 * Class for clients to access. Because we know this service always runs in
	 * the same process as its clients, we don't need to deal with IPC.
	 */
	public class LocalBinder extends Binder {
		public LocationService getService(Context context,String sActivity) {
			sActivityGlobal = sActivity;
			contextGlobal = context;
			
			if(lalinksGeoLocationObj == null){
				//lalinksGeoLocationObj = LalinksGeoLocation.getInstance(getApplicationContext());
				lalinksGeoLocationObj = new LalinksGeoLocation(contextGlobal);
			}
			
			startLocationService();
			
			return LocationService.this;
		}
	}

	@Override
	public void onCreate() {
		// mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

		// Display a notification about us starting. We put an icon in the
		// status bar.
		
	}
	


	public int onStartCommand(Intent intent, int flags, int startId) {
		if(true)Log.e("LocalService", "Received start id " + startId + ": " + intent);
		// We want this service to continue running until it is explicitly
		// stopped, so return sticky.
		//return START_STICKY;
		 return 1;
	}

	@Override
	public void onDestroy() {
		// Cancel the persistent notification.
		// mNM.cancel(R.string.local_service_started);

		if(lalinksGeoLocationObj != null){
			lalinksGeoLocationObj.removeUpdatesAndStopAll();
		}
		// Tell the user we stopped.
		//Toast.makeText(this, "Service Destroied",Toast.LENGTH_SHORT).show();
	}

	@Override
	public IBinder onBind(Intent intent) {
		
		return mBinder;
	}

	// This is the object that receives interactions from clients. See
	// RemoteService for a more complete example.
	private final IBinder mBinder = new LocalBinder();

	/**
	 * Show a notification while this service is running.
	 */
	private void startLocationService() {
		// In this sample, we'll use the same text for the ticker and the
		// expanded notification
		// CharSequence text = getText(R.string.local_service_started);

		// Set the icon, scrolling text and timestamp
		// Notification notification = new Notification(R.drawable.stat_sample,
		// text,
		// System.currentTimeMillis());

		// The PendingIntent to launch our activity if the user selects this
		// notification
		// PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
		// new Intent(this, LocalServiceActivities.Controller.class), 0);

		// Set the info for the views that show in the notification panel.
		// notification.setLatestEventInfo(this,
		// getText(R.string.local_service_label),
		// text, contentIntent);

		// Send the notification.
		// We use a layout id because it is a unique number. We use it later to
		// cancel.
		// mNM.notify(R.string.local_service_started, notification);

		lalinksGeoLocationObj.getLocation(contextGlobal,
				new LalinksGeoLocation.LocationResult() {

					@Override
					public void gotLocation(Location location) {
						// TODO Auto-generated method stub
						

						if(location != null){
							
							Intent toBroadcast = new Intent(Intent.ACTION_VIEW);
							toBroadcast.putExtra("location", location);
							sendBroadcast(toBroadcast);
							
							previousLocation = location;
						
						}
							


					}
				});

	}


    ////// location


}

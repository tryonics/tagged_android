package com.tryonics.android.taggedlk.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.tryonics.android.taggedlk.model.Promotion.Promotion;
import com.tryonics.android.taggedlk.model.Promotion.PromotionRow;


import java.util.LinkedList;
import java.util.List;

/**
 * Created by dimalperera on 7/30/14.
 */

public class PromotionAdapter extends BaseAdapter {

    private Context ctx;
    private List<Promotion> placeList = new LinkedList<Promotion>();
    private PromotionItemOnClick promotionItemOnClick;

    public PromotionAdapter(final Context mContextValue) {
        super();
        this.ctx = mContextValue;
    }

    public void appendPlaceList(final List<Promotion> placeListValue){
        this.placeList.addAll(placeListValue);
        notifyDataSetChanged();
    }

    public void clearPlaceList(){
        this.placeList.clear();
        notifyDataSetChanged();
    }

    public void setOnItemOnclick(final PromotionItemOnClick catItemOnClick) {
        this.promotionItemOnClick = catItemOnClick;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (placeList != null) {
            count = placeList.size();
        }
        return count;
    }

    @Override
    public Promotion getItem(int position) {
        return placeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        PromotionRow catRowView = null;

        if (convertView == null) {
            catRowView = new PromotionRow(ctx);
        } else {
            catRowView = (PromotionRow) convertView;
        }
        catRowView.update(placeList.get(position));
        catRowView.setOnItemOnclick(promotionItemOnClick);

        return catRowView;
    }

    public interface PromotionItemOnClick {
        void onClickItem(final Promotion Place);
    }
}


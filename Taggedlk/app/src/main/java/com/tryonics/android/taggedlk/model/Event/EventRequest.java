package com.tryonics.android.taggedlk.model.Event;

import android.content.Context;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.tryonics.android.taggedlk.model.Promotion.PromotionResponse;
import com.tryonics.android.taggedlk.util.Constant;

import java.util.HashMap;


/**
 * Created by dimalperera on 7/30/14.
 */
public class EventRequest extends
        SpringAndroidSpiceRequest<EventResponse> {

    private String offset;
    private String url = Constant.BASE_URL + "event/list";


    public EventRequest(Context mContext, String offset) {
        super(EventResponse.class);
        this.offset = offset;


//        Log.e("offset", offset);
/*        Log.e("latitude", String.valueOf(location.getLatitude()));
        Log.e("longitude", String.valueOf(location.getLongitude()));*/

    }

    @Override
    public EventResponse loadDataFromNetwork() throws Exception {


        EventResponse eventResponse;


            HashMap<String, String> params = new HashMap<String, String>();
            params.put("offset", offset);
           /* params.put("latitude", String.valueOf(location.getLatitude()));
            params.put("longitude", String.valueOf(location.getLongitude()));*/
            eventResponse = getRestTemplate().getForObject(url, EventResponse.class);


        return eventResponse;
    }

    public String createCacheKey() {
        return "EventResponse/"+offset;
    }
}

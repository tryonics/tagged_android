package com.tryonics.android.taggedlk.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.tryonics.android.taggedlk.model.Event.Event;
import com.tryonics.android.taggedlk.model.Event.EventRow;
import com.tryonics.android.taggedlk.model.Promotion.Promotion;
import com.tryonics.android.taggedlk.model.Promotion.PromotionRow;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by dimalperera on 7/30/14.
 */

public class EventAdapter extends BaseAdapter {

    private Context ctx;
    private List<Event> eventList = new LinkedList<Event>();
    private EventItemOnClick promotionItemOnClick;

    public EventAdapter(final Context mContextValue) {
        super();
        this.ctx = mContextValue;
    }

    public void appendPlaceList(final List<Event> eventList){
        this.eventList.addAll(eventList);
        notifyDataSetChanged();
    }

    public void clearPlaceList(){
        this.eventList.clear();
        notifyDataSetChanged();
    }

    public void setOnItemOnclick(final EventItemOnClick catItemOnClick) {
        this.promotionItemOnClick = catItemOnClick;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (eventList != null) {
            count = eventList.size();
        }
        return count;
    }

    @Override
    public Event getItem(int position) {
        return eventList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        EventRow catRowView = null;

        if (convertView == null) {
            catRowView = new EventRow(ctx);
        } else {
            catRowView = (EventRow) convertView;
        }
        catRowView.update(eventList.get(position));
        catRowView.setOnItemOnclick(promotionItemOnClick);

        return catRowView;
    }

    public interface EventItemOnClick {
        void onClickItem(final Event event);
    }
}


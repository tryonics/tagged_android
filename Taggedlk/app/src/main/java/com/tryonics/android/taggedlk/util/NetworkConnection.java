package com.tryonics.android.taggedlk.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkConnection {

	private Context context;
	private NetworkConnectionStatus networkConnectionStatus;

	public NetworkConnection(final Context applicationContext) {
		this.context = applicationContext;
	}

	private void isConnectingToInternet(final Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (NetworkInfo xx : info) {
					if (xx.getState() == NetworkInfo.State.CONNECTED) {
						networkConnectionStatus.connectionSuccessful();
						return;
					}
				}
			}
		}
		networkConnectionStatus.connectionFail();
	}

	public void setListener(
			final NetworkConnectionStatus networkConnectionStatus) {
		this.networkConnectionStatus = networkConnectionStatus;
		this.isConnectingToInternet(context);
	}

	public interface NetworkConnectionStatus {
		void connectionSuccessful();

		void connectionFail();
	}

}

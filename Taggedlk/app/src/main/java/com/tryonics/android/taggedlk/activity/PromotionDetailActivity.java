/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tryonics.android.taggedlk.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.tryonics.android.taggedlk.R;
import com.tryonics.android.taggedlk.model.Promotion.Promotion;
import com.tryonics.android.taggedlk.model.promotiondetail.PromotionDetailRequest;
import com.tryonics.android.taggedlk.model.promotiondetail.PromotionDetailResponse;
import com.tryonics.android.taggedlk.util.Constant;

public class PromotionDetailActivity extends Activity {

    TextView textView;
    private PromotionDetailRequest request;
    private SpiceManager spiceManager = new SpiceManager(JacksonSpringAndroidSpiceService.class);
    private String lastRequestCacheKey;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection_demo);

        textView = (TextView) findViewById(R.id.text1);

        Promotion promotion = getIntent().getExtras().getParcelable(Constant.PROMOTION);

        if(promotion != null){
            //Toast.makeText(getApplicationContext(), promotion.getPromotionId(), Toast.LENGTH_LONG).show();
            performRequest(promotion);
        }
    }

    private void performRequest(Promotion promotion) {
        request = new PromotionDetailRequest(getApplicationContext(), promotion);
        spiceManager.execute(request, lastRequestCacheKey,
                DurationInMillis.ALWAYS_EXPIRED, new PromotionDetailRequestListener());
    }

    @Override
    public void onStart() {
        if (!spiceManager.isStarted()) {
            spiceManager.start(getApplicationContext());
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (spiceManager.isStarted()) {
            spiceManager.shouldStop();
        }
        super.onStop();
    }

    private class PromotionDetailRequestListener implements RequestListener<PromotionDetailResponse> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Toast.makeText(getApplicationContext(), spiceException.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onRequestSuccess(PromotionDetailResponse promotionDetailResponse) {

            Log.e("zdf", "v" + promotionDetailResponse.getPromotionDetail().getDescription());
            textView.setText(Html.fromHtml(promotionDetailResponse.getPromotionDetail().getDescription()));
        }
    }
}
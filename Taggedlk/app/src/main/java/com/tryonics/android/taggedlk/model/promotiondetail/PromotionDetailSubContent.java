package com.tryonics.android.taggedlk.model.promotiondetail;

import android.os.Parcel;
import android.os.Parcelable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by tryonics on 10/7/2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PromotionDetailSubContent implements Parcelable {

    @JsonProperty("title")
    private String subContentTitle;
    @JsonProperty("description")
    private String subContentDescription;

    public String getSubContentTitle() {
        return subContentTitle;
    }

    public void setSubContentTitle(String subContentTitle) {
        this.subContentTitle = subContentTitle;
    }

    public String getSubContentDescription() {
        return subContentDescription;
    }

    public void setSubContentDescription(String subContentDescription) {
        this.subContentDescription = subContentDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.subContentTitle);
        dest.writeString(this.subContentDescription);
    }

    public PromotionDetailSubContent() {
    }

    private PromotionDetailSubContent(Parcel in) {
        this.subContentTitle = in.readString();
        this.subContentDescription = in.readString();
    }

    public static final Parcelable.Creator<PromotionDetailSubContent> CREATOR = new Parcelable.Creator<PromotionDetailSubContent>() {
        public PromotionDetailSubContent createFromParcel(Parcel source) {
            return new PromotionDetailSubContent(source);
        }

        public PromotionDetailSubContent[] newArray(int size) {
            return new PromotionDetailSubContent[size];
        }
    };
}

package com.tryonics.android.taggedlk.model.Event;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.tryonics.android.taggedlk.R;
import com.tryonics.android.taggedlk.adapter.EventAdapter;
import com.tryonics.android.taggedlk.adapter.PromotionAdapter;

/**
 * Created by tryonics on 9/29/2014.
 */
public class EventRow extends LinearLayout {


    private LinearLayout linearLayoutLPlaceRoot;
    private ImageView promotionRowImage;
    private TextView eventTitle, eventDateTime, eventPlace, eventDistance;
    // private ProgressBar progressBar;
   private EventAdapter.EventItemOnClick promotionOnItemOnClick;
    private Event event;
    public static DisplayImageOptions options;
    private ImageLoader imageLoader;


    public EventRow(final Context context) {
        super(context);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_launcher)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));

        inflateView(context);
    }

    private void inflateView(final Context context) {
        LayoutInflater.from(context).inflate(R.layout.row_event, this);
        this.linearLayoutLPlaceRoot = (LinearLayout) this.findViewById(R.id.root_promotion);
        this.promotionRowImage = (ImageView) this
                .findViewById(R.id.iv_promotion);

        this.eventTitle = (TextView) this.findViewById(R.id.tvEventTitle);
        this.eventDateTime = (TextView) this.findViewById(R.id.tvEventDateTime);
        this.eventPlace = (TextView) this.findViewById(R.id.tvEventPlace);
        this.eventDistance = (TextView) this.findViewById(R.id.tvEventDistance);
        // this.progressBar = (ProgressBar) this.findViewById(R.id.progressBar);
        linearLayoutLPlaceRoot.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (promotionOnItemOnClick != null) {
                    //linearLayoutLPlaceRoot.setBackgroundColor(Color.RED);
                    promotionOnItemOnClick.onClickItem(event);
                }
            }
        });

    }

    public void update(final Event event) {

        if (event != null) {
            this.event = event;
        }
        if (event.getTitle() != null) {
            eventTitle.setText(event.getTitle());
        } else {
            eventTitle.setText("");
        }

        if (event.getDate() != null && event.getTime() != null) {
            eventDateTime.setText(event.getDate()+" "+event.getTime());
        } else {
            eventDateTime.setText("");
        }

        if (event.getPlace() != null) {
            eventPlace.setText(event.getPlace());
        } else {
            eventPlace.setText("");
        }

        if (event.getDistance() != null) {
            eventDistance.setText(event.getDistance());
        } else {
            eventTitle.setText("");
        }

    }

    public void setOnItemOnclick(final EventAdapter.EventItemOnClick mPlaceItemOnClick) {
        this.promotionOnItemOnClick = mPlaceItemOnClick;
    }


}

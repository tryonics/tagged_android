package com.tryonics.android.taggedlk.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.tryonics.android.taggedlk.R;
import com.tryonics.android.taggedlk.activity.PromotionDetailActivity;
import com.tryonics.android.taggedlk.adapter.EventAdapter;
import com.tryonics.android.taggedlk.adapter.PromotionAdapter;
import com.tryonics.android.taggedlk.model.Event.Event;
import com.tryonics.android.taggedlk.model.Event.EventRequest;
import com.tryonics.android.taggedlk.model.Event.EventResponse;
import com.tryonics.android.taggedlk.model.Promotion.Promotion;
import com.tryonics.android.taggedlk.model.Promotion.PromotionRequest;
import com.tryonics.android.taggedlk.model.Promotion.PromotionResponse;
import com.tryonics.android.taggedlk.util.NetworkConnection;

/**
 * Created by tryonics on 9/29/2014.
 */
public class EventFragment extends Fragment {

    private PullToRefreshListView eventPullToRefreshListView;
    private SpiceManager spiceManager = new SpiceManager(JacksonSpringAndroidSpiceService.class);
    private EventAdapter eventAdapter;
    private EventRequest request;
    private String lastRequestCacheKey;
    private String offset;
    private EventFragmentOnClick eventFragmentOnClick;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_event, container, false);

        initUIComponents(rootView);
        // Demonstration of a collection-browsing activity.
        /*rootView.findViewById(R.id.demo_collection_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), PromotionDetailActivity.class);
                        startActivity(intent);
                    }
                });

        // Demonstration of navigating to external activities.
        rootView.findViewById(R.id.demo_external_activity)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Create an intent that asks the user to pick a photo, but using
                        // FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET, ensures that relaunching
                        // the application from the device home screen does not return
                        // to the external activity.
                        Intent externalActivityIntent = new Intent(Intent.ACTION_PICK);
                        externalActivityIntent.setType("image*//*");
                        externalActivityIntent.addFlags(
                                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        startActivity(externalActivityIntent);
                    }
                });*/

        return rootView;
    }

    private void initUIComponents(View rootView) {


        eventPullToRefreshListView = (PullToRefreshListView)rootView.findViewById(R.id.eventPullToRefreshListView);
        eventPullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(
                    final PullToRefreshBase<ListView> refreshView) {

                NetworkConnection networkConnection = new NetworkConnection(getActivity());
                networkConnection.setListener(new NetworkConnection.NetworkConnectionStatus() {
                    @Override
                    public void connectionSuccessful() {
                        offset = "0"; //Set off as zero to get init request
                        eventAdapter.clearPlaceList();
                        //promotionFragmentOnClick.PullToDownRefresh(category);
                    }

                    @Override
                    public void connectionFail() {
                        eventPullToRefreshListView.onRefreshComplete();
                        // Crouton.makeText(getActivity(), "No internet connection", Style.ALERT).show();
                    }
                });

            }

            @Override
            public void onPullUpToRefresh(
                    final PullToRefreshBase<ListView> refreshView) {

                NetworkConnection networkConnection = new NetworkConnection(getActivity());
                networkConnection.setListener(new NetworkConnection.NetworkConnectionStatus() {
                    @Override
                    public void connectionSuccessful() {
                        // performRequest(category, currentLocation, offset);
                    }

                    @Override
                    public void connectionFail() {
                        eventPullToRefreshListView.onRefreshComplete();
                        //Crouton.makeText(getActivity(), "No internet connection", Style.ALERT).show();
                    }
                });
            }

        });

        eventAdapter = new EventAdapter(getActivity());
        eventPullToRefreshListView.setAdapter(eventAdapter);

        eventAdapter.setOnItemOnclick(new EventAdapter.EventItemOnClick() {
            @Override
            public void onClickItem(Event event) {

            }
         });
        /* emptyView = new EmptyView(getActivity());
        pullToRefreshListView.setEmptyView(emptyView);*/

        /* if(category != null)
            placeAdapter.clearPlaceList();*/
                //Toast.makeText(getActivity(), "XXX", Toast.LENGTH_SHORT).show();
        performRequest(offset);
    }

    public interface EventFragmentOnClick {
        public void EventOnClick(Event event);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            eventFragmentOnClick = (EventFragmentOnClick) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    private void performRequest(String offset) {
        request = new EventRequest(getActivity(), offset);
        lastRequestCacheKey = request.createCacheKey();
        spiceManager.execute(request, lastRequestCacheKey,
                DurationInMillis.ALWAYS_EXPIRED, new EventRequestListener());
    }

    private class EventRequestListener implements RequestListener<EventResponse> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {

        }

        @Override
        public void onRequestSuccess(EventResponse eventResponse) {
            if (eventResponse.getEvent() == null) {
                return;
            }

            //pullToRefreshListView.setScrollEmptyView(true);

            eventAdapter.appendPlaceList(eventResponse.getEvent());

            offset = eventResponse.getOffset();

            if (offset == null) {
                eventPullToRefreshListView.onRefreshComplete();
                eventPullToRefreshListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
            } else {
                eventPullToRefreshListView.onRefreshComplete();
                eventPullToRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);
            }
        }


    }

    @Override
    public void onStart() {
        if (!spiceManager.isStarted()) {
            spiceManager.start(getActivity());
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (spiceManager.isStarted()) {
            spiceManager.shouldStop();
        }
        super.onStop();
    }

}
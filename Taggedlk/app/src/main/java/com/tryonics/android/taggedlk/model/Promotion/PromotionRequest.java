package com.tryonics.android.taggedlk.model.Promotion;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.tryonics.android.taggedlk.util.Constant;

import java.util.HashMap;


/**
 * Created by dimalperera on 7/30/14.
 */
public class PromotionRequest extends
        SpringAndroidSpiceRequest<PromotionResponse> {

    private String offset;
    private Location location;
    private String placeUrlWithoutCategory = Constant.BASE_URL + "promotion/list?latitude={latitude}&longitude={longitude}&offset={offset}";


    public PromotionRequest(Context mContext, Location location, String offset) {
        super(PromotionResponse.class);
        this.offset = offset;
        this.location = location;

        Log.e("offset", offset);
        Log.e("latitude", String.valueOf(location.getLatitude()));
        Log.e("longitude", String.valueOf(location.getLongitude()));

    }

    @Override
    public PromotionResponse loadDataFromNetwork() throws Exception {

        PromotionResponse placeResponse;

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("offset", offset);
        params.put("latitude", String.valueOf(location.getLatitude()));
        params.put("longitude", String.valueOf(location.getLongitude()));

        placeResponse = getRestTemplate().getForObject(placeUrlWithoutCategory, PromotionResponse.class, params);
        return placeResponse;
    }

    public String createCacheKey() {
        return "PromotionResponse/"+offset;
    }
}

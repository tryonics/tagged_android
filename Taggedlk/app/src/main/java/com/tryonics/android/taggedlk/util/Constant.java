package com.tryonics.android.taggedlk.util;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.tryonics.android.taggedlk.R;


/**
 * Created by dimalperera on 8/2/14.
 */
public class Constant {

    public Constant() {

    }

    //public static String BASE_URL = "http://192.168.1.110/projects/tag/public/v1/";
    public static String BASE_URL = "http://192.168.1.203/tagged_web/tag/public/v1/";

    public static boolean DEBUG = true;

    public static DisplayImageOptions options;

    public static DisplayImageOptions Options(){
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img_promotion_default)
                .showImageForEmptyUri(R.drawable.img_promotion_default)
                .showImageOnFail(R.drawable.img_promotion_default)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer(20))
                .build();

        return options;

    }

    public static String PROMOTION = "PROMOTION";
}

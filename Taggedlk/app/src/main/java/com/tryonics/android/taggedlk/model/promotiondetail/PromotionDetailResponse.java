package com.tryonics.android.taggedlk.model.promotiondetail;

import android.os.Parcel;
import android.os.Parcelable;

import com.tryonics.android.taggedlk.model.Error;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by tryonics on 10/7/2014.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PromotionDetailResponse implements Parcelable {

    @JsonProperty("error")
    private com.tryonics.android.taggedlk.model.Error error;
    @JsonProperty("prromotion")
    private PromotionDetail promotionDetail;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public PromotionDetail getPromotionDetail() {
        return promotionDetail;
    }

    public void setPromotionDetail(PromotionDetail promotionDetail) {
        this.promotionDetail = promotionDetail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.error, 0);
        dest.writeParcelable(this.promotionDetail, 0);
    }

    public PromotionDetailResponse() {
    }

    private PromotionDetailResponse(Parcel in) {
        this.error = in.readParcelable(com.tryonics.android.taggedlk.model.Error.class.getClassLoader());
        this.promotionDetail = in.readParcelable(PromotionDetail.class.getClassLoader());
    }

    public static final Parcelable.Creator<PromotionDetailResponse> CREATOR = new Parcelable.Creator<PromotionDetailResponse>() {
        public PromotionDetailResponse createFromParcel(Parcel source) {
            return new PromotionDetailResponse(source);
        }

        public PromotionDetailResponse[] newArray(int size) {
            return new PromotionDetailResponse[size];
        }
    };
}
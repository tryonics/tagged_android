package com.tryonics.android.taggedlk.model.Event;

import android.os.Parcel;
import android.os.Parcelable;

import com.tryonics.android.taggedlk.model.*;
import com.tryonics.android.taggedlk.model.Error;
import com.tryonics.android.taggedlk.model.Promotion.Promotion;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;

/**
 * Created by tryonics on 9/30/2014.
 */
public class EventResponse implements Parcelable {

    @JsonProperty("error")
    private com.tryonics.android.taggedlk.model.Error error;
    @JsonProperty("events")
    private ArrayList<Event> event;
    @JsonProperty("offset")
    private String offset;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public ArrayList<Event> getEvent() {
        return event;
    }

    public void setEvent(ArrayList<Event> event) {
        this.event = event;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.error, 0);
        dest.writeSerializable(this.event);
        dest.writeString(this.offset);
    }

    public EventResponse() {
    }

    private EventResponse(Parcel in) {
        this.error = in.readParcelable(com.tryonics.android.taggedlk.model.Error.class.getClassLoader());
        this.event = (ArrayList<Event>) in.readSerializable();
        this.offset = in.readString();
    }

    public static final Parcelable.Creator<EventResponse> CREATOR = new Parcelable.Creator<EventResponse>() {
        public EventResponse createFromParcel(Parcel source) {
            return new EventResponse(source);
        }

        public EventResponse[] newArray(int size) {
            return new EventResponse[size];
        }
    };
}

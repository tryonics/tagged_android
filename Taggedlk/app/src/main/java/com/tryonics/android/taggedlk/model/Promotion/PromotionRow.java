package com.tryonics.android.taggedlk.model.Promotion;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.tryonics.android.taggedlk.R;
import com.tryonics.android.taggedlk.adapter.PromotionAdapter;

/**
 * Created by tryonics on 9/29/2014.
 */
public class PromotionRow extends LinearLayout {


    private LinearLayout linearLayoutLPlaceRoot;
    private ImageView promotionRowImage;
    private TextView promotionTitle, promotionDescription, promotionDistance;
    // private ProgressBar progressBar;
   private PromotionAdapter.PromotionItemOnClick promotionOnItemOnClick;
    private Promotion place;
    public static DisplayImageOptions options;
    private ImageLoader imageLoader;


    public PromotionRow(final Context context) {
        super(context);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img_promotion_default)
                .showImageForEmptyUri(R.drawable.img_promotion_default)
                .showImageOnFail(R.drawable.img_promotion_default)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));

        inflateView(context);
    }

    private void inflateView(final Context context) {
        LayoutInflater.from(context).inflate(R.layout.row_promotion, this);
        this.linearLayoutLPlaceRoot = (LinearLayout) this.findViewById(R.id.root_promotion);
        this.promotionRowImage = (ImageView) this
                .findViewById(R.id.iv_promotion);

        this.promotionTitle = (TextView) this.findViewById(R.id.tv_title);
        this.promotionDescription = (TextView) this.findViewById(R.id.tv_description);
        this.promotionDistance = (TextView) this.findViewById(R.id.tv_distance);
        // this.progressBar = (ProgressBar) this.findViewById(R.id.progressBar);
        linearLayoutLPlaceRoot.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (promotionOnItemOnClick != null) {
                    //linearLayoutLPlaceRoot.setBackgroundColor(Color.RED);
                    promotionOnItemOnClick.onClickItem(place);
                }
            }
        });

    }

    public void update(final Promotion promotion) {

        if (promotion != null) {
            place = promotion;
        }
        if (promotion.getTitle() != null) {
            promotionTitle.setText(promotion.getTitle());
        } else {
            promotionTitle.setText("");
        }

        if (promotion.getDescription() != null) {
            promotionDescription.setText(promotion.getDescription());
        } else {
            promotionDescription.setText("");
        }


        if (promotion.getDistance() != null) {
            promotionDistance.setText(promotion.getDistance());
        } else {
            promotionTitle.setText("");
        }


        if (promotion.getImageUrl() != null) {
            imageLoader.displayImage(promotion.getImageUrl(),
                    promotionRowImage, options,
                    new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingComplete(String imageUri,View view, Bitmap loadedImage) {
                            // Do whatever you want with Bitmap
                        }
                    }
            );
        }




    }

    public void setOnItemOnclick(final PromotionAdapter.PromotionItemOnClick mPlaceItemOnClick) {
        this.promotionOnItemOnClick = mPlaceItemOnClick;
    }


}

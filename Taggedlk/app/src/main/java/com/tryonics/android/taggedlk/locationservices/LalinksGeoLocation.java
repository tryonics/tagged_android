/*
 * Copyright 2011 Cyber LMJ
 *
 * @author Dulan Dissanayake
 * @version 1.0
 * 
 * 
 */

package com.tryonics.android.taggedlk.locationservices;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import java.util.TimerTask;

public class LalinksGeoLocation{
	
	private final int DIALOG_LOCATION_SETTINGS = 0;
	
    //Timer timer1;
    private LocationManager lm;
    private LocationResult locationResult;
    private boolean gps_enabled=false;
    private boolean network_enabled=false;
    private Context mContext;
    
    private Location lastKnowLocation = null;

    private String TAG = "LalinksGeoLocation";
    
    //public static LalinksGeoLocation lalinksGeoLocationObj = null;
    
    
//    public static LalinksGeoLocation getInstance(Context mContext){
//    	
//        if (lalinksGeoLocationObj == null) {
//        	lalinksGeoLocationObj = new LalinksGeoLocation(mContext);
//        }
//
//        
//        return lalinksGeoLocationObj;
//    }
    

    public LalinksGeoLocation(Context context) {
		// TODO Auto-generated constructor stub
    	
    	mContext = context;
    	
    	if(lm==null){
    		lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    	}
    	
    	if(!isLocationsServiceEnabled()){
    		//Open the Popup for display the location settings messsage
    		createGpsDisabledAlert();
    	}
            
	}
    
    public boolean isLocationsServiceEnabled(){
    	boolean bIsServiceEnable = false;
    	
    	 //exceptions will be thrown if provider is not permitted.
        try{
        	gps_enabled=lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        	
        	}catch(Exception ex){
        		ex.printStackTrace();
        	}
        try{
        	network_enabled=lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        	
        	}catch(Exception ex){
        		ex.printStackTrace();
        	}

        //don't start listeners if no provider is enabled
        if(!gps_enabled && !network_enabled){

            bIsServiceEnable = false;
        }else{
        	bIsServiceEnable = true;
        }
    	
    	
    	return bIsServiceEnable;
    }

    public boolean getLocation(Context context, LocationResult result)
    {
        //I use LocationResult callback class to pass location value from MyLocation to user code.
        locationResult=result;
        mContext = context;
        
        if(true) Log.e(TAG, TAG);


        if(gps_enabled)
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,25, locationListenerGps);
        if(network_enabled)
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 25, locationListenerNetwork);
        //timer1=new Timer();
        //timer1.schedule(new GetLastLocation(), 200000);
    
        //Getting the last Known Location

        if(network_enabled){
            lastKnowLocation = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            locationResult.gotLocation(lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
        }

        if(gps_enabled){
        	lastKnowLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        	locationResult.gotLocation(lm.getLastKnownLocation(LocationManager.GPS_PROVIDER));
        }
        
        return true;
    }

    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
        	//Toast.makeText(mContext, "GPS Location Changed GPS_PROVIDER", Toast.LENGTH_SHORT).show();
            //timer1.cancel();
        	if(lastKnowLocation != null){
        		if(lastKnowLocation.getAccuracy() < location.getAccuracy()){
        			locationResult.gotLocation(location);
        		}else{
        			locationResult.gotLocation(lastKnowLocation);
        		}
        	}else{
        		locationResult.gotLocation(location);
        	}
            
        	//locationResult.gotLocation(location);
            //lm.removeUpdates(this);
            //lm.removeUpdates(locationListenerNetwork);
        }
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
        	//Toast.makeText(mContext, "GPS Location Changed NETWORK_PROVIDER", Toast.LENGTH_SHORT).show();
            //timer1.cancel();
        	if(lastKnowLocation != null){
        		if(lastKnowLocation.getAccuracy() < location.getAccuracy()){
        			locationResult.gotLocation(location);
        		}else{
        			locationResult.gotLocation(lastKnowLocation);
        		}
        	}else{
        		locationResult.gotLocation(location);
        	}
        	//locationResult.gotLocation(location);
            //lm.removeUpdates(this);
            //lm.removeUpdates(locationListenerGps);
        }
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    public void removeUpdatesAndStopAll(){
    	//timer1.cancel();
    	if(true)Log.e(TAG, "Updates Stoped");
        lm.removeUpdates(locationListenerGps);
        lm.removeUpdates(locationListenerNetwork);
    }
    
    public void addLocationListners(){
    	
    }
    
////////////////////////////////////////////////////////////////////////////////////
    private void createGpsDisabledAlert(){  
    	AlertDialog.Builder builder = new AlertDialog.Builder(mContext);  
    	builder.setMessage("enable_location_service")
    	     .setCancelable(false)  
    	     .setPositiveButton("string_settings",
    	          new DialogInterface.OnClickListener(){  
    	          public void onClick(DialogInterface dialog, int id){  
    	               showGpsOptions();  
    	          }  
    	     });  
    	     builder.setNegativeButton("string_cancel",
    	          new DialogInterface.OnClickListener(){  
    	          public void onClick(DialogInterface dialog, int id){  
    	               dialog.cancel();  
    	          }  
    	     });  
    	AlertDialog alert = builder.create();  
    	
    	alert.show();  
    	} 
    
    private void showGpsOptions(){  
        Intent gpsOptionsIntent = new Intent(  
                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);  
        mContext.startActivity(gpsOptionsIntent);  
}  

    
 //////////////////////////////////////////////////////////////////////////////////////////   
    
    
    class GetLastLocation extends TimerTask {
        @Override
        public void run() {
             //lm.removeUpdates(locationListenerGps);
             //lm.removeUpdates(locationListenerNetwork);
        	//Toast.makeText(mContext, "GetLastLocation Timer Task", Toast.LENGTH_SHORT).show();
            //Log.e("Test","RUN"); 
        	Location net_loc=null, gps_loc=null;
             if(gps_enabled)
                 gps_loc=lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
             if(network_enabled)
                 net_loc=lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

             //if there are both values use the latest one
             if(gps_loc!=null && net_loc!=null){
                 if(gps_loc.getTime()>net_loc.getTime())
                     locationResult.gotLocation(gps_loc);
                 else
                     locationResult.gotLocation(net_loc);
                 return;
             }

             if(gps_loc!=null){
                 locationResult.gotLocation(gps_loc);
                 return;
             }
             if(net_loc!=null){
                 locationResult.gotLocation(net_loc);
                 return;
             }
             locationResult.gotLocation(null);
        }
    }

    public static abstract class LocationResult{
        public abstract void gotLocation(Location location);
    }

}
